const TicketControll = require('../models/ticketControll');
const ticketControll = new TicketControll();

const socketController = ( socket ) => {

    socket.emit('last-ticket', ticketControll.lastTicket);
    socket.emit('current-status', ticketControll.last4Tickets);
    socket.emit('pending-tickets', ticketControll.tickets.length);
    
    socket.on('next-ticket', ( payload, callback ) => {
        
        const next = ticketControll.next();
        callback( next );

        socket.broadcast.emit('pending-tickets', ticketControll.tickets.length);

    });

    socket.on('attended-ticket', ({ desk }, callback ) => {
        
        if ( !desk ) {
            return callback({
                ok: false,
                message: 'The desk is required!'
            });
        }

        const ticket = ticketControll.attendTicket( desk );

        socket.broadcast.emit('current-status', ticketControll.last4Tickets);
        socket.emit('pending-tickets', ticketControll.tickets.length);
        socket.broadcast.emit('pending-tickets', ticketControll.tickets.length);

        if ( !ticket ) {
            callback({
                ok: false,
                message: 'There are no pending tickets.'
            });
        } else {
            callback({
                ok: true,
                ticket
            });
        }

    });
}

module.exports = {
    socketController
}