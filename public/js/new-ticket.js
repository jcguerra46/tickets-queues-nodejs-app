// HTML References
const lblNewTicket  = document.querySelector('#lblNewTicket');
const createBtn = document.querySelector('button');

const socket = io();

socket.on('connect', () => {

    console.log('Connected');
    createBtn.disabled = false;

});

socket.on('disconnect', () => {

    console.log('Disconnected');
    createBtn.disabled = true;

});

socket.on('last-ticket', ( lastTicket ) => {
    lblNewTicket.innerText = 'Ticket ' + lastTicket;
});

createBtn.addEventListener( 'click', () => {

    socket.emit('next-ticket', null, ( ticket ) => {
        lblNewTicket.innerText = ticket;
    });

});