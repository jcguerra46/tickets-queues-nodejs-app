// HTML References
const lblDesk = document.querySelector('h1');
const attendBtn = document.querySelector('button');
const lblTicket = document.querySelector('small');
const alertDiv = document.querySelector('.alert');
const lblPendingTickets = document.querySelector('#lblPendingTickets');

const searchParams = new URLSearchParams( window.location.search );

if ( !searchParams.has( 'desk' ) ) {
    window.location = 'index.html';
    throw new Error('The desk is required!');
}

const desk = searchParams.get('desk');
lblDesk.innerText = desk;
alertDiv.style.display = 'none';
const socket = io();

socket.on('connect', () => {

    console.log('Connected');
    attendBtn.disabled = false;

});

socket.on('disconnect', () => {

    console.log('Disconnected');
    attendBtn.disabled = true;

});

socket.on('pending-tickets', ( pendingTickets ) => {
    
    if ( pendingTickets === 0 ) {
        lblPendingTickets.style.display = 'none';
    } else {
        lblPendingTickets.style.display = '';
        lblPendingTickets.innerText = pendingTickets;
    }

});

attendBtn.addEventListener( 'click', () => {

    socket.emit('attended-ticket', { desk }, ({ ok, ticket, message }) => {

        if( !ok ) {
            lblTicket.innerText = 'no one.';
            return alertDiv.style.display = '';
        }

        lblTicket.innerText = `Ticket ${ ticket.number }`;
    });

});

