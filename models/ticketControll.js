const path = require('path');
const fs = require('fs');

const Ticket = require('./ticket');

class TicketControll {

    constructor() {

        this.lastTicket = 0;
        this.today = new Date().getDate();
        this.tickets = [];
        this.last4Tickets = [];

        this.init();

    }

    get toJson() {
        return {
            lastTicket: this.lastTicket,
            today: this.today,
            tickets: this.tickets,
            last4Tickets: this.last4Tickets
        }
    }

    init() {

        const { lastTicket, today, tickets, last4Tickets } = require('../database/data.json');
        if ( today === this.today ) {
            this.tickets = tickets;
            this.lastTicket = lastTicket;
            this.last4Tickets = last4Tickets;
        } else {
            // is other day
            this.saveDatabase();
        }

    }

    saveDatabase() {

        const databasePath = path.join( __dirname, '../database/data.json' );
        fs.writeFileSync( databasePath, JSON.stringify( this.toJson ) );

    }

    next() {

        this.lastTicket += 1;
        const ticket = new Ticket( this.lastTicket, null );
        this.tickets.push( ticket );

        this.saveDatabase();
        return `Ticket ${ ticket.number }`;

    }

    attendTicket( desk ) {

        // validate if exist a ticket in the queue
        if ( this.tickets.length === 0 ) return null;

        // get first ticket and delete from the tickets array
        const ticket = this.tickets.shift(); // this.tickets[0];

        // Assign desk 
        ticket.desk = desk;

        // insert this ticket to last4Tickets array
        this.last4Tickets.unshift( ticket );

        // validate that alway exist 4 tickets in last4Tickets array
        if ( this.last4Tickets.length > 4 ) this.last4Tickets.splice(-1,1);
        
        this.saveDatabase();

        return ticket;
    }

}

module.exports = TicketControll;