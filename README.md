### Tickets Queues NodeJS APP ###
With this application you can control the order of customer service in an office, tickets that indicate the service number, tells you when and where you will be attended.

## Steps
First that all, clone this repo 
```sh
$ git clone https://jcguerra46@bitbucket.org/jcguerra46/tickets-queues-nodejs-app.git
```

Copy globals environment file in your main directory
```sh
$ cp .env.example .env
```

Install all dependencies
```sh
$ npm install
```

## Run the application
```sh
$ npm start
```

## Author
Juan Carlos Guerra 
```sh
email: juancarlosguerra46@gmail.com
```